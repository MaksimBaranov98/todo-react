export enum urlLocations {
    Login = "/login",
    Home = "/home",
    ToDo = "/todo",
}
