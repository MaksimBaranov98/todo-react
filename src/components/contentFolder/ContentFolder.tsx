import React, { useState } from 'react';
import axios from 'axios';
import { ReactComponent as EditSvg } from 'assests/image/edit.svg';
import { ReactComponent as AddSvg } from 'assests/image/add.svg';
import TaskRow from './TaskRow';
import { Route } from 'react-router-dom';

import { SideBarMenuFolder } from 'interfaces/Sidebar';
import { StatusToDo } from 'interfaces/Tasks';
import './ContentFolder.scss';

interface ContentFolderProps {
    selectedFolder: SideBarMenuFolder;
	setSelectedFolder: (folder: SideBarMenuFolder) => void;
	folders: SideBarMenuFolder[];
}

interface RenderContentFolder {
	folder: SideBarMenuFolder;
}

const ContentFolder = (props: ContentFolderProps) => {
    const { selectedFolder, setSelectedFolder, folders } = props;
    const [newTaskActive, setNewTaskActive] = useState<boolean>(false);
    const [newTaskName, setNewTaskName] = useState<string>('');
    const onEdit = () => {
        const newName = window.prompt('Input a name for the new folder');
        if (newName) {
            const updatedFolder = {
                ...selectedFolder,
                name: newName,
            }
            setSelectedFolder(updatedFolder);
            axios.patch(`http://localhost:3002/lists/${selectedFolder.id}`, {
                name: newName,
            }).catch(() => {
                alert('Failed to update folder name');
            });
        }
    }
    const rendferTaskRow = () => {
        return (
            <>
                {selectedFolder.tasks.map((task) => (
                    <TaskRow
                        key={task.id}
                        task={task}
                    />
                ))
                }
            </>
        )
    }
    const noTasksMassage = () => {
        return (
            <div className='contentFolder__no-tasks'>
                You have no tasks
            </div>
        )
    }
    const toggleNewTask = () => {
        setNewTaskActive(!newTaskActive);
    }
    const cancelClick = () => {
        toggleNewTask();
        setNewTaskName('');
    }
    const addNewTask = () => {
        if (!newTaskName) {
            alert('Input task name');
            return;
        }
        const newTask = {
            status: StatusToDo.needToDo,
            dateCreation: new Date().getTime(),
            name: newTaskName,
            listId: selectedFolder.id,
        }
        axios.post(`http://localhost:3002/tasks`, newTask).then(({ data }) => {
            const updatedFolder = {
                ...selectedFolder,
                tasks: [...selectedFolder.tasks, {
                    ...data,
                }],
            }
            setSelectedFolder(updatedFolder);
        }).catch(() => {
            alert('Failed to added new task');
        }).then(() => {
            setNewTaskActive(false);
            setNewTaskName('');
        });
    }
    const onChangeNameNewTask = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;
        setNewTaskName(value);
    }
    const newTasks = () => {
        return (
            <div className="new-task">
                {!newTaskActive ?
                    <div
                        className="new-task__no-active"
                        onClick={toggleNewTask}
                    >
                        <AddSvg />
                        <span>
                            Create New Task
                        </span>
                    </div>
                    :
                    <div className="new-task__active">
                        <input
                            type="text"
                            placeholder="Input task name"
                            className="field"
                            onChange={onChangeNameNewTask}
                        >

                        </input>
                        <button
                            disabled={!newTaskName}
                            className="new-task__active__button"
                            onClick={addNewTask}
                        >
                            Add task
                        </button>
                        <button
                            className="new-task__active__button grey"
                            onClick={cancelClick}
                        >
                            Cacel
                        </button>
                    </div>
                }
            </div>
        )
	}

	const RenderContentFolder = (props: RenderContentFolder) => {
		const { folder } = props;
    console.log(folder);

		return (
			<div className="contentFolder">
				<h2 className='contentFolder__title' style={{ color: folder.color.hex }} >
					{folder.name}
					<EditSvg onClick={onEdit} />
				</h2>
				{folder.tasks.length ? rendferTaskRow() : noTasksMassage()}
				{newTasks()}
			</div>
		);
	}
    return (
        <>
			<Route exact path='/'>
				{
					folders.map((folder) => {
						return (
							<RenderContentFolder folder={folder} key={+folder.id} />
						)
					})
				}
			</Route>
			<Route path='/folder/:id'>
				<RenderContentFolder folder={selectedFolder}/>
			</Route>
		</>
    );
};

export default ContentFolder;
