import React, { useState } from "react";
import { ReactComponent as CheckSvg } from "assests/image/check.svg";

import { ToDo } from "interfaces/Tasks";
import "./ContentFolder.scss";

interface TaskRowProps {
    task: ToDo;
}

const TaskRow = (props: TaskRowProps) => {
    const { task } = props;
    const { name: nameProp, dateCreation } = task;
    const [name, setName] = useState<string>(nameProp);
    const onChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;
        setName(value);
    };
    return (
        <div className="contentFolder__items">
            <div className="checkbox">
                <input id={`check${dateCreation}`} type="checkbox"></input>
                <label htmlFor={`check${dateCreation}`}>
                    <CheckSvg />
                </label>
            </div>
            <input value={name} onChange={onChangeName} />
        </div>
    );
};

export default TaskRow;
