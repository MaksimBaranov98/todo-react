import React, { useState } from "react";
import axios from "axios";
import Menu from "./Menu";
import NewFolderButton from "./NewFolderButton";
import Header from "./Header";

import { SideBarMenuFolder, ColorsBadge } from "interfaces/Sidebar";
import "./SideBar.scss";

interface SideBarProps {
    folders: SideBarMenuFolder[];
    colors: ColorsBadge[];
    selectedFolder: SideBarMenuFolder;
    setSelectedFolder: (folder: SideBarMenuFolder) => void;
}

const SideBar = (props: SideBarProps) => {
    const { folders, colors, selectedFolder, setSelectedFolder } = props;
    const [folderList, setFolderList] = useState<SideBarMenuFolder[]>(folders);
    const addNewfolder = (value: SideBarMenuFolder) => {
        setFolderList([...folderList, value]);
    };
    const removeFolder = (id: string) => {
        axios.delete(`http://localhost:3002/lists/${id}`).then(() => {
            const newFolderList = folderList.filter(
                (folder) => folder.id !== id
            );
            setFolderList(newFolderList);
        });
    };
    return (
        <>
            <div className="sidebar">
                <ul className="list">
                    <Header />
                    <Menu
                        folders={folderList}
                        removeFolder={removeFolder}
                        selectedFolder={selectedFolder}
                        setSelectedFolder={setSelectedFolder}
                    />
                    <NewFolderButton
                        colors={colors}
                        addNewfolder={addNewfolder}
                    />
                </ul>
            </div>
        </>
    );
};

export default SideBar;
