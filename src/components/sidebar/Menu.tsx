import React from "react";
import classNames from "classnames";
import { ReactComponent as RemoveSvg } from "assests/image/remove.svg";
import Badge from "./Badge";
import { useHistory } from 'react-router-dom';

import { SideBarMenuFolder } from "interfaces/Sidebar";
import "./SideBar.scss";

interface SideBarMenuProps {
    folders: SideBarMenuFolder[];
    selectedFolder: SideBarMenuFolder;
    removeFolder: (id: string) => void;
    setSelectedFolder: (folder: SideBarMenuFolder) => void;
}

const Menu = (props: SideBarMenuProps) => {
    const history = useHistory();
    const { folders, selectedFolder, removeFolder, setSelectedFolder } = props;
    const onRemoveClick = (id: string) => {
        removeFolder(id);
    };
    return (
        <>
            {folders.map((folder, idx) => {
                return (
                    <li
                        key={idx}
                        onClick={() => {
                            history.push(`/folder/${folder.id}`);
                            setSelectedFolder(folder)
                        }}
                        className={classNames({
                            active: +selectedFolder.id === +folder.id,
                        })}
                    >
                        <Badge color={folder.color.name} />
                        <span className="folder-name">
                            {folder.name}
                            <span>{` (${folder.tasks.length})`}</span>
                        </span>
                        <RemoveSvg
                            className="remove-icon"
                            onClick={() => onRemoveClick(folder.id)}
                        />
                    </li>
                );
            })}
        </>
    );
};

export default Menu;
