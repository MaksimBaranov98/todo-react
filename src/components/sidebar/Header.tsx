import React from "react";
import { ReactComponent as ListSvg } from "assests/image/list.svg";
import { useHistory } from 'react-router-dom';

import "./SideBar.scss";

const SideBar: React.FC = () => {
    const history = useHistory();
    return (
        <li className={`name`} onClick={() => {
            history.push('/');
        }}>
            <i>
                <ListSvg />
            </i>
            <span>All Todos</span>
        </li>
    );
};

export default SideBar;
