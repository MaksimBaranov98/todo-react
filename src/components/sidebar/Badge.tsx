import React from "react";
import "./SideBar.scss";

interface BadgeProps {
    color: string;
    onClick?: () => void;
    className?: string | boolean;
}

const Badge = (props: BadgeProps) => {
    const { color, onClick, className } = props;
    return (
        <i className={`badge badge--${color} ${className}`} onClick={onClick} />
    );
};

export default Badge;
