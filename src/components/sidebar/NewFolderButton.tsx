import React, { useState, useEffect } from "react";
import axios from "axios";
import { ReactComponent as AddSvg } from "assests/image/add.svg";
import { ReactComponent as CloseSvg } from "assests/image/close.svg";
import Badge from "./Badge";

import { ColorsBadge } from "interfaces/Sidebar";
import { SideBarMenuFolder } from "interfaces/Sidebar";
import "./SideBar.scss";
import "../../style.scss";

interface NewFolderButtonProps {
    colors: any;
    addNewfolder: (value: SideBarMenuFolder) => void;
}

const NewFolderButton = (props: NewFolderButtonProps) => {
    const { addNewfolder, colors } = props;
    const [showFolderForm, setShowFolderForm] = useState(false);
    const [allColor, setAllColor] = useState<ColorsBadge[]>(colors);
    const [selectedColor, setSelectedColor] = useState<number>();
    const [folderName, setFolderName] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const onToggleFolderForm = () => {
        setShowFolderForm(!showFolderForm);
        setFolderName("");
    };
    const onChoiseColor = (id: number) => {
        setSelectedColor(id);
    };
    useEffect(() => {
        setAllColor(colors);
        if (colors.length) {
            setSelectedColor(colors[0].id);
        }
    }, [colors]);
    const onAddNewfolder = () => {
        if (!folderName) {
            alert("Input name folder");
            return;
        }
        setIsLoading(true);
        axios
            .post("http://localhost:3002/lists", {
                name: folderName,
                colorId: selectedColor,
            })
            .then(({ data }) => {
                const newFolder = {
                    ...data,
                    color: {},
                    tasks: [],
                };
                axios
                    .get(`http://localhost:3002/colors/${selectedColor}`)
                    .then(({ data: color }) => {
                        newFolder.color = color;
                        addNewfolder(newFolder);
                        onToggleFolderForm();
                    });
            })
            .finally(() => {
                setIsLoading(false);
            });
    };
    const onChangeNameNewFolder = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;
        setFolderName(value);
    };
    const newFolderForm = () => {
        return (
            <div className="list__new-folder-form">
                <CloseSvg
                    className="close-button"
                    onClick={onToggleFolderForm}
                />
                <input
                    type="text"
                    placeholder="Name Folder"
                    className="field"
                    onChange={onChangeNameNewFolder}
                />
                <div className="list__colors">
                    {allColor.map((item, idx) => {
                        return (
                            <Badge
                                key={idx}
                                color={item.name}
                                onClick={() => onChoiseColor(item.id)}
                                className={
                                    selectedColor === item.id && "active"
                                }
                            />
                        );
                    })}
                </div>
                <button
                    className="list__add-button-form"
                    onClick={onAddNewfolder}
                >
                    {isLoading ? "Adding..." : "Add"}
                </button>
            </div>
        );
    };
    return (
        <>
            <li className="list__add-button" onClick={onToggleFolderForm}>
                <i>
                    <AddSvg />
                </i>
                <span>Add Folder</span>
            </li>
            {showFolderForm && newFolderForm()}
        </>
    );
};

export default NewFolderButton;
