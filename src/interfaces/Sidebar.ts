import { ToDo } from "interfaces/Tasks";

export interface SideBarMenuFolder {
    id: string;
    name: string;
    colorId: number;
    color: ColorsBadge;
    tasks: ToDo[];
}

export interface ColorsBadge {
    id: number;
    name: string;
    hex: string;
}
