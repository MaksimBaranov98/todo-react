export enum StatusToDo {
    needToDo = "needToDo",
    inProgress = "inProgress",
    completed = "completed",
}

export interface ToDo {
    id: string;
    name: string;
    // descriptions: string;
    status: StatusToDo;
    dateCreation: number;
    listId: number;
}

export const RequestStatusString = {
    [StatusToDo.needToDo]: "Need To Do",
    [StatusToDo.inProgress]: "In progress",
    [StatusToDo.completed]: "Completed",
};
