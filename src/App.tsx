import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from 'react-router-dom';

import { ContentFolder, SideBar } from "components";

import { SideBarMenuFolder, ColorsBadge } from "interfaces/Sidebar";
import "./style.scss";

const App: React.FC = () => {
    const history = useHistory();
    const [folders, setFolders] = useState<SideBarMenuFolder[]>();
    const [colors, setColors] = useState<ColorsBadge[]>();
    const [selectedFolder, setSelectedFolder] = useState<SideBarMenuFolder>();
    const [fetchData, setFetchData] = useState(false);
    useEffect(() => {
        Promise.all([
            axios.get("http://localhost:3002/lists?_expand=color&_embed=tasks"),
            axios.get("http://localhost:3002/colors"),
        ])
            .then(([folders, colors]) => {
                setFolders(folders.data);
                setColors(colors.data);
                setSelectedFolder(folders.data[0]);
            })
            .finally(() => {
                setTimeout(() => {
                    setFetchData(true);
                }, 0);
            });
    }, []);
    useEffect(() => {
        const folderId = history.location.pathname;
        const id = folderId.split('folder/')[1];
        if (folders?.length) {
            const selectedFolder = folders!.find((folder) => +folder.id === +id);
            setSelectedFolder(selectedFolder);
        }
    }, [folders, history.location.pathname]);
    return (
        <>
            <div className="todo">
                {fetchData ? (
                    <>
                        <SideBar
                            folders={folders!}
                            colors={colors!}
                            selectedFolder={selectedFolder!}
                            setSelectedFolder={setSelectedFolder}
                        />
                        <ContentFolder
                            selectedFolder={selectedFolder!}
                            setSelectedFolder={setSelectedFolder}
                            folders={folders!}
                        />
                    </>
                ) : (
                    <div>Loading...</div>
                )}
            </div>
        </>
    );
};

export default App;
