# Todo React

## To start the project use

- Install the project `npm i`

- Start the project `npm start`

- Start the json-server `npm run start-json`

- Format the style code `npm run format`

## Info

- TypeScript
- React
- MobX/Redux?
- Webpack
- Eslint
